import 'package:flutter/material.dart';

import 'package:flutter_marvel/model/department_model.dart';
import 'package:flutter_marvel/model/school_model.dart';
import 'package:flutter_marvel/view/contact_view.dart';

import 'package:url_launcher/url_launcher.dart';

class DepartmentView extends StatefulWidget {

 //final DepartmentModel departmentModel;
 //  final ContactModel contactModel;
  final SchoolModel schoolModel;
    //final ContactModel contactModel;
  
  DepartmentView({this.schoolModel});

  @override
  _DepartmentViewState createState() => _DepartmentViewState();
}

class _DepartmentViewState extends State<DepartmentView> {

   List<DepartmentModel> deptList = new List<DepartmentModel>();

  @override
  void initState() {
 
    super.initState();
    print(widget.schoolModel.department);
    deptList = departmentList.where((data) => data.name == widget.schoolModel.department).toList();
  }

  final List<DepartmentModel>departmentList = [
    DepartmentModel(name: 'CSE'),
    DepartmentModel(name: 'ECE'),
    DepartmentModel(name: 'BGE'),
    DepartmentModel(name: 'FWT'),
    DepartmentModel(name: 'ICT CELL'),
    DepartmentModel(name: 'Law'),
    DepartmentModel(name: 'FMRT'),

  ];

   
 



  @override
  Widget build(BuildContext context) {
    


    return Scaffold(
        backgroundColor: Theme
            .of(context)
            .primaryColor,
        appBar: AppBar(
          elevation: .5,
          title: Text('Ku Contact App'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                print("hello search");
              },
            )
          ],
        ),
     
        body: ListView.builder(
            itemCount: deptList.length,
            itemBuilder: (context, index) {
              return InkWell(
                onTap: (){
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context)=>ContactView(departmentModel: deptList[index],)
                    )
                  );
                },
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(10),
                  child: Card(
                    margin: EdgeInsets.only(left: 5,right: 5,top: 2,bottom: 3),
                    elevation: 4,
                    child: ListTile(
                      leading: CircleAvatar(
                        child: Text(
                          deptList[index].name.substring(0,1),
                              style: TextStyle(
                            fontWeight: FontWeight.bold,
                                fontSize: 20
                        ),
                        ),
                      ),
                      title: Text(deptList[index].name),
                    )
                  ),
                ),
              );
            }));
  }

  launchCall(number) async {
    var url = 'tel:$number';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }




}